@extends('layouts.main')

@section('sidebar')
  
  <!-- Blog Entries Column -->
  <div class="col-md-8">
    <div class="d-flex justify-content-between">
      <h1>@yield('title','Home Page')</h1>
      
      @section('sorting')
        @include('layouts.sorting')
      @show
    </div>
    <hr>
  @section('content')
      @include('layouts.errors')
      @foreach($post as $key => $value)
          <!-- Blog Post -->
          <div class="card mb-4" id="accordion">
            <img class="card-img-top" data-toggle="modal" data-target="#exampleModal" src="http://placehold.it/750x300" alt="Card image cap">
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-body justify-content-center">
                    <img src="http://placehold.it/750x300" alt="Card image cap">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-header" id="heading{{$value->id}}">
              <a data-toggle="collapse" data-target="#collapse{{$value->id}}" aria-expanded="true" aria-controls="collapseOne" href="#"><h2>{{$value->title}}</h2>
              </a>
            </div>
            <div class="card-body" id="collapse{{$value->id}}" class="collapse show" aria-labelledby="heading{{$value->id}}" data-parent="#accordion">
              <p>{!!str_limit($value->content, $limit = 200, $end = '...')!!}</p>
              
              <a href="{{route('posts.show',['slug'=>str_slug($value->title,'-'),'id'=>$value->id])}}" class="btn btn-primary">
              Read More &rarr;</a>
              
            </div>
            <div class="card-footer text-muted">
              Posted on {{$value->created_at}} by
              <a href="{{route('users.show',['id'=>$value->users->id])}}">{{$value->users->name}}</a>
            </div>
          </div>
      @endforeach
    @show
    </div>
      <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

          <!-- Search Widget -->
          <div class="card my-4">
            <h5 class="card-header">Search</h5>
            <div class="card-body">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                  <button class="btn btn-secondary" type="button">Go!</button>
                </span>
              </div>
            </div>
          </div>

          <!-- Categories Widget -->
          <div class="card my-4">
            <h5 class="card-header">Categories</h5>
            <div class="card-body">
              <div class="row">
                <div class="container">
                <div class="col">
                  <ul class="list-unstyled mb-0">
                    @foreach($category as $key => $value)
                    <li>
                      <a href="{{route('posts.category',['slug'=>$value->slug])}}">{{$value->category_name}}</a>
                    </li>
                    @endforeach
                  </ul>
                </div>
              </div>
              </div>
            </div>
          </div>

          <!-- Side Widget -->
          <div class="card my-4">
            <h5 class="card-header">Side Widget</h5>
            <div class="card-body">
            
                <a class="btn btn-primary" href="{{route('posts.create')}}">New Post</a>
              
            </div>
          </div>

        </div>
  <script type="text/javascript">
    $('.dropdown-toggle').dropdown()
  </script>
@endsection
