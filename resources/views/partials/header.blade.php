<header>
    <div class="navbar navbar-dark bg-dark box-shadow">
      <div class="container d-flex justify-content-end">
        <a href="{{route('posts.index')}}" class="navbar-brand d-flex align-items-center">
          <strong>Home</strong>
        </a>
        
          @guest
            
            <div><a class="navbar-brand" href="{{ route('login') }}"><strong>Login</strong></a></div>
            <div><a class="navbar-brand" href="{{ route('register') }}"><strong>Register</strong></a></div>
          @else
          <div class="dropdown">
            <a id="navbarDropdown" class="navbar-brand dropdown-toggle  align-items-center" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
              <strong>{{ Auth::user()->name }}</strong> <span class="caret"></span>
            </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ route('users.show',['id'=>Auth::user()->id]) }}">
                        {{ __('Personal blog') }}
                </a>
                 
                <a class="dropdown-item" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
                  </form>
              </div>
          </div>
          @endguest
        </a>
      </div>
    </div>
  </header>