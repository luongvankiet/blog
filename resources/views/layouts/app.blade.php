<!DOCTYPE html>
<html>
<head>
	<title>@yield('title')</title>
  <link rel="stylesheet" href="{{asset('css/style.css')}}">
	<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('css/bootstrap-grid.min.css')}}">
	<link rel="stylesheet" href="{{asset('css/bootstrap-reboot.min.css')}}">
	<link rel="stylesheet" href="{{asset('css/album.css')}}">
  <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Fonts -->
  <link rel="dns-prefetch" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <style>
  /* Make the image fully responsive */
  .carousel-inner img {
      width: 100%;
      height: 100%;
  }
  </style>
</head>
<body>


    <header>
      <div class="navbar navbar-dark bg-dark box-shadow">
        <div class="container d-flex justify-content-end">
          <a href="{{route('home')}}" class="navbar-brand d-flex align-items-center">
            <strong>Home</strong>
          </a>
          
            @guest
              <div><a class="navbar-brand" href="{{ route('user.login') }}"><strong>Login</strong></a></div>
              <div><a class="navbar-brand" href="{{ route('register') }}"><strong>Register</strong></a></div>
            @else
            <div class="dropdown">
              <a id="navbarDropdown" class="navbar-brand dropdown-toggle  align-items-center" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                <strong>{{ Auth::user()->name }}</strong> <span class="caret"></span>
              </a>
                
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="{{ route('profile') }}"
                      onclick="">
                          {{ __('User info') }}
                  </a>
                   
                  <a class="dropdown-item" href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
                          {{ __('Logout') }}
                  </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
                    </form>
                </div>
            </div>
            @endguest
          </a>
        </div>
      </div>
      
    </header>
    
    <main role="main">

    
    <!-- Page Content -->
    <div class="container main">

      <div class="row">

        
    @section('sidebar')
      <div id="demo" class="carousel slide" data-ride="carousel">
          <ul class="carousel-indicators">
            <li data-target="#demo" data-slide-to="0" class="active"></li>
            <li data-target="#demo" data-slide-to="1"></li>
            <li data-target="#demo" data-slide-to="2"></li>
          </ul>
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img src="{{asset('img\la.jpg')}}" alt="Los Angeles" width="1100" height="500">
              <div class="carousel-caption">
                <h3>Los Angeles</h3>
                <p>We had such a great time in LA!</p>
              </div>   
            </div>
            <div class="carousel-item">
              <img src="{{asset('img\chicago.jpg')}}" alt="Chicago" width="1100" height="500">
              <div class="carousel-caption">
                <h3>Chicago</h3>
                <p>Thank you, Chicago!</p>
              </div>   
            </div>
            <div class="carousel-item">
              <img src="{{asset('img\ny.jpg')}}" alt="New York" width="1100" height="500">
              <div class="carousel-caption">
                <h3>New York</h3>
                <p>We love the Big Apple!</p>
              </div>   
            </div>
          </div>
          <a class="carousel-control-prev" href="#demo" data-slide="prev">
            <span class="carousel-control-prev-icon"></span>
          </a>
          <a class="carousel-control-next" href="#demo" data-slide="next">
            <span class="carousel-control-next-icon"></span>
          </a>
        </div>
    @show

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->
    </div>
  </main>
	<script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
	<script src="{{asset('js/bootstrap.min.js')}}"></script>
  <script src="{{asset('js/bootstrap.js')}}"></script>
</body>
</html>