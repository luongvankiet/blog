<div class="dropdown">
	<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	  Sort by
	</button>
	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
		<form id="date" action="{{route('posts.sort',['type'=>'date'])}}" method="POST">
			@csrf
		  <a class="dropdown-item" onclick="document.getElementById('date').submit();">Latest post</a>
		</form>
	  
		<a class="dropdown-toggle dropdown-item" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		Users
		</a>
		<div class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
		@foreach($users as $u)
		<form id="sort-form{{$u->id}}" method="POST" action="{{route('posts.sort',['type'=>'users'])}}">
			@csrf
			<a class="dropdown-item" onclick="document.getElementById('sort-form{{$u->id}}').submit();">{{$u->name}}</a>
			<input type="hidden" name="user_id" value="{{$u->id}}">
		</form>
		@endforeach
		</div>
	</div>
</div>

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
@csrf
</form>