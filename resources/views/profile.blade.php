@extends('layouts.main')
@section('sidebar')
  <div class="col-md-8">
 @foreach($user_post as $key => $value)
    <div class=" d-flex justify-content-between" style="margin-bottom: 10px;">
      <h2>{{$value->users->name}} posts</h2>
      @if(Auth::user()->id == $user->id)
      <a class="btn btn-primary" href="{{route('posts.create')}}">Create New Post</a>
      @endif
    </div>
        <!-- Blog Post -->
        <div class="card mb-4" id="accordion">
          <img class="card-img-top" data-toggle="modal" data-target="#exampleModal" src="http://placehold.it/750x300" alt="Card image cap">
          <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-body justify-content-center">
                  <img src="http://placehold.it/750x300" alt="Card image cap">
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
          <div class="card-header" id="heading{{$value->id}}">
            <a data-toggle="collapse" aria-expanded="false" aria-controls="collapse{{$value->id}}" href="#collapse{{$value->id}}" role="button"><h2>{{$value->title}}</h2>
            </a>
          </div>
          <div class="card-body" id="collapse{{$value->id}}" class="collapse">
            <p>{!!str_limit($value->content, $limit = 200, $end = '...')!!}</p>
              @if(Auth::user()->id == $value->users->id)
              <a href="{{route('posts.show',['slug'=>str_slug($value->title,'-'), 'id'=>$value->id])}}" class="btn btn-primary" style="margin-right: 5px;">Read More</a>
              <a href="{{route('posts.edit', ['id' => $value->id])}}" class="btn btn-primary" style="margin-right: 5px;">Edit</a>
              <a href="#" class="btn btn-primary">Delete</a>
              
              @else
              <a href="{{route('posts.show',['slug'=>str_slug($value->title,'-'), 'id'=>$value->id])}}" class="btn btn-primary" style="margin-right: 5px;">Read More</a>
              @endif
          </div>
          <div class="card-footer text-muted">
            Posted on {{$value->created_at}} by
            <a href="{{route('users.show',['id'=>$value->users->id])}}">{{$value->users->name}}</a>
          </div>
        </div>
    @endforeach
  </div>
  <!-- Sidebar Widgets Column -->

    <div class="col-md-4">
      <!-- Categories Widget -->
      <div class="card my-4" id="accordion">
        <div class="media mb-4" data-toggle="popover" title="{{$user->name}}" data-content="{{str_limit($user->email, 17, "...")}}"  style="margin-top: 20px; margin-left: 20px;">
            <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/100x100" alt="">
            <div class="media-body" style="margin-left: 10px;">
              <h3>{{$user->name}}</h3>
              <h5>{{str_limit($user->email, 17, "...")}}</h5>
          </div>
        </div>
      </div>

      <!-- Side Widget -->
      <div class="card my-4">
        <h5 class="card-header">Side Widget</h5>
        <div class="card-body">
          <a class="btn btn-primary" href="{{route('posts.create')}}">New Post</a>
        </div>
      </div>
    </div>
@endsection