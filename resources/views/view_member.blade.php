@extends('layouts.main')
@section('title', 'Profile')
@section('topic','Member')
@section('sidebar')
  <div class="container main">
    <div class="row justify-content-center">
    @foreach($data as $key => $value)
    <div class="col-md-8">
      <div class="card mb-4">
        <div class="card-body">
          <div class="image"><img src="..\{{$value->img}}">
          </div> 
          <div class="text">
            Name: {{$value->name}}
            </br>
            Nickname: {{$value->nickname}}
            </br>
            Email: {{$value->email}}
            </br>
            Date of birth: {{date('d-m-Y', strtotime($value->date_of_birth))}}
          </div>
        </div>
      </div>
    </div>
    @endforeach
  </div>
</div>
@endsection