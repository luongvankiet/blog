@extends('home')
@section('content')

<!-- Title -->
  @foreach($post as $key => $value)

    @include('layouts.errors')
    @section('title',$value->title)
    <!-- Author -->
      <p class="lead">
        by
        <a href="{{route('users.show',['id'=>$value->users->id])}}">{{$value->users->name}}</a>
      </p>
    <hr>

    <!-- Date/Time -->
  <div class="row" style="margin-bottom: 5px;">
    <div class="col">
      <p>Posted on {{$value->created_at}}</p>
    </div>
    @if(Auth::user()->id == $value->users->id)
    <div class="col col-lg-1">
      <a href="{{route('posts.edit', ['id' => $value->id])}}" class="btn btn-primary" style="margin-right: 5px;">Edit</a>
    </div>
    <div class="col col-lg-2">
      {!! Form::open(['method' => 'DELETE', 'route' => ['posts.destroy', $value->id]]) !!}
            {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
      {!! Form::close() !!}
    </div>
    @endif
  </div>

    <!-- Preview Image -->
    <img class="img-fluid rounded" src="http://placehold.it/900x300" alt="">

    <hr>
    <!-- Post Content -->
    <p class="lead text-justify">{!!$value->content!!}</p>

    <blockquote class="blockquote">
      <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
      <footer class="blockquote-footer">Someone famous in
        <cite title="Source Title">Source Title</cite>
      </footer>
    </blockquote>


    <hr>

    <!-- Comments Form -->
    <div class="card my-4">
      <h5 class="card-header">Leave a Comment:</h5>
      <div class="card-body">
        <form method="POST" action="{{route('comments.store',['post'=>$value->slug])}}">
          @csrf
          <div class="form-group">
            <textarea class="form-control" rows="3"  name="content" placeholder="Write something about this post ..." required></textarea>
            <input type="hidden" name="slug" value="{{$value->slug}}">
          </div>
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>
    </div>
    <!-- Single Comment -->
    @foreach($value->comments as $comment)
    <div class="media mb-4">
      <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
      <div class="media-body">
        <h4 class="mt-0"><a href="{{route('users.show',['id'=>$comment->users->id])}}">{{$comment->users->name}}</a></h4>
        {{$comment->content}}
        <p class="card-text"><small class="text-muted">{{$comment->created_at->diffForHumans()}}</small></p>
      </div>


    </div>
    <!-- Comment with nested comments -->
    <!-- <div class="media mb-4">
      <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
      <div class="media-body">
        <h5 class="mt-0">Commenter Name</h5>
        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
      </div>
    </div> -->
    @endforeach
  @endforeach
@endsection