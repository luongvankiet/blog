@extends('home')

@section('content')
	
  @foreach($post as $key => $value)
  @section('title', $value->categories->category_name)
          <!-- Blog Post -->
          <div class="card mb-4" id="accordion">
            <img class="card-img-top" data-toggle="modal" data-target="#exampleModal" src="http://placehold.it/750x300" alt="Card image cap">
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-body justify-content-center">
                    <img src="http://placehold.it/750x300" alt="Card image cap">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-header" id="heading{{$value->id}}">
              <a data-toggle="collapse" data-target="#collapse{{$value->id}}" aria-expanded="true" aria-controls="collapseOne" href="#"><h2>{{$value->title}}</h2>
              </a>
            </div>
            <div class="card-body" id="collapse{{$value->id}}" class="collapse show" aria-labelledby="heading{{$value->id}}" data-parent="#accordion">
              <p>{!!str_limit($value->content, $limit = 200, $end = '...')!!}</p>

              <a href="{{route('posts.show',['slug'=>str_slug($value->title,'-'),'id'=>$value->id])}}" class="btn btn-primary">Read More &rarr;</a>
            </div>
            <div class="card-footer text-muted">
              Posted on {{$value->created_at}} by
              <a href="{{route('users.show',['id'=>$value->users->id])}}">{{$value->users->name}}</a>
            </div>
          </div>
      @endforeach
@endsection