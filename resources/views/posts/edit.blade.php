@extends('home')
@section('title','New Post')

@section('content')
    <div class="card mb-4">
      <div class="card-body">
      @foreach($post as $key => $value)
      	@include('layouts.errors')
		{!! Form::model($value, ['route' => ['posts.update', $value->id], 'method'=>'PUT']) !!}
			{{ Form::label('title','Post title') }}
			{{Form::text('title',null, array('class'=>'form-control'))}}

			{{ Form::label('content','Post title', array('style'=> 'margin-top: 10px;')) }}
			{{ Form::textarea('content', null, array('class'=>'form-control', 'rows'=>5, 'id'=>'editor')) }}


			<div class="form-group" style="margin-top: 10px;"">
                <label>Category</label>
                <select  name="category" class="form-control">
                    @foreach($category as $key => $value)
                    	<option value="{{ $value->id }}">{{$value->category_name}}</option>
                    @endforeach

                </select>
            </div>

            <div class="row">
              <div class="col-md-6">
				{{ Form::submit('Save changes', array('class'=>'btn btn-primary btn-block', 'style'=> 'margin-top: 10px;'))}}
			  </div>
			  <div class="col-md-6">
				{!!Html::linkRoute('users.show', 'Cancel',array(Auth::user()->id), array('class'=>'btn btn-primary btn-block', 'style'=> 'margin-top: 10px;'))!!}
			  </div>
			</div>
		{!! Form::close() !!}
	  @endforeach
	  </div>
	</div>
@endsection