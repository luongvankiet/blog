<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::group(['prefix' => 'admin', 'middleware' => ['role:admin']], function() {
//     Route::get('/', 'AdminController@welcome');
//     Route::get('/manage', ['middleware' => ['permission:manage-admins'], 'uses' => 'AdminController@manageAdmins']);
// });
// Route::prefix('admin')->group(function(){
// 	Route::get('login', 'Admin\LoginController@showLoginForm')->name('admin.login');
//     Route::post('login', 'Admin\LoginController@login');
//     Route::post('logout', 'Admin\LoginController@logout')->name('admin.logout');

//         // Registration Routes...
//     Route::get('register', 'Admin\RegisterController@showRegistrationForm')->name('admin.register');
//     Route::post('register', 'Admin\RegisterController@register');

//         // Password Reset Routes...
//     Route::get('password/reset','Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
//     Route::post('password/email', 'Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
//     Route::get('password/reset/{token}', 'Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');
//     Route::post('password/reset', 'Admin\ResetPasswordController@reset');

//     Route::get('/','Admin\AdminController@index')->name('admin.home');
// });
Route::prefix('users')->middleware('web')->group(function(){
	// Auth::routes();
	//login
	Route::get('/login', ['as'=>'login','uses'=>'User\LoginController@showLoginForm']);
	Route::post('/login', ['as'=>'login','uses'=>'User\LoginController@processLogin']);
	//logout
	Route::post('/logout', ['as'=>'logout','uses'=>'User\LoginController@logout']);
	//register
	Route::get('/register', ['as'=>'register','uses'=>'User\RegisterController@showRegisterForm']);
	Route::post('/register', ['as'=>'register','uses'=>'User\RegisterController@processRegister']);
	//Password maintain
	Route::get('password/reset','User\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'User\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'User\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'User\ResetPasswordController@reset');

});
Route::resource('users','UsersController');
Route::resource('posts','PostsController', ['except' => 'show'])->middleware('auth');
Route::prefix('posts')->middleware('auth')->group(function(){
	Route::resource('/{post}/comments','CommentsController');
    Route::get('/category/{slug}','PostsController@showPostByCategory')->name('posts.category');
    Route::get('/{slug}/{id}','PostsController@show')->name('posts.show');
    Route::post('/{type}','PostsController@SortedPost')->name('posts.sort');
});

// Route::get('home',['as'=>'home', 'uses'=>'MyController@home']);



//Route::get('view/', ['as'=>'view','uses'=>'UsersController@index']);



// Route::get('/dtus1', function(){
//     $faker = Faker\Factory::create();
//     $limit = 100;
//     $posts = [];
//     for ($i = 0; $i < $limit; $i++) {
//     	$title = $faker->text(200);
//     	$slug = str_slug($title, '-');
//         $posts[$i] = [
//             'id'            => $faker->numberBetween(1,100),
//             'title'			=>$title,
//             'content'       =>$faker->paragraph,
//             'slug'			=>$slug,
//             'rate'			=>$faker->numberBetween($min = 0, $max = 100),
//             'active'		=>true,
//             'id_user'       => $faker->numberBetween(1,100),
//             'id_category'	=>$faker->numberBetween(1,100),
//         ];
//     }
//     return response()->json($posts);
// });


Route::get('/','HomeController@home')->name('layouts.home');

// Route::get("/dtus", function()
// {
//     $post = Post::where('id',1)->get();
//     foreach($post as $p)
//         echo $p->created_at;
//  //    dd($dt);
// 	// return view('Post');
// });

Route::get('/home', 'HomeController@home')->name('home');
// Route::get('/app', 'HomeController@index')->name('home');