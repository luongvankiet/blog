<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Category extends Model
{
    protected $table = 'categories';

    public function posts()
    {
        return $this->hasMany('App\Post','category_id','id');
    }

    public static function findPostByCategory($slug)
    {
        return Post::whereHas('categories', function($q) use ($slug){
            $q -> where('slug', $slug);
        })->paginate(3);
    }
}
