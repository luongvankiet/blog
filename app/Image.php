<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'images';

    protected $fillable = [
    	'image', 'user_id', 'post_id', 'comment_id'
    ];

    public function users(){
    	return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function posts(){
    	return $this->belongsTo('App\Post', 'post_id', 'id');
    }

    public function comments(){
    	return $this->belongsTo('App\Comment', 'comment_id', 'id');
    }
}
