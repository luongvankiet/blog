<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
    protected $table = 'comments';
    protected $primaryKey = 'id';
    protected $fillable = [
        'content', 'post_id', 'user_id'
    ];

    public function users()
    {
    	return $this->belongsTo('App\User','user_id','id');
    }

    public function posts()
    {
    	return $this->belongsTo('App\Post', 'post_id', 'id');
    }

    public function images()
    {
        return $this->hasMany('App\Image', 'comment_id', 'id');
    }

}
