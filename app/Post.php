<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;
use Carbon\Carbon;
class Post extends Model
{
    //
    protected $table = 'posts';
    protected $primaryKey = 'id';
    protected $fillable = [
        'title', 'content','slug', 'rate', 'active', 'user_id', 'category_id'
    ];

    protected $rules = [
        'title' => 'required|max:255',
        'content' =>'required',
        'user_id' => 'required',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $errors;

    public function getCreatedAtAttribute($timestamp) {
        return Carbon::parse($timestamp)->format('m-d-Y');
    }

    
    public function categories(){
    	return $this->belongsTo('App\Category','category_id','id');
    }

    public function comments()
    {
    	return $this->hasMany('App\Comment','post_id','id');
    }

    public function users()
    {
    	return $this->belongsTo('App\User','user_id','id');
    }

    public function images()
    {
        return $this->hasMany('App\Image', 'post_id', 'id');
    }

    public static function findUserByPost($id){
        return Post::with('users')->where('id',$id)->get();
    }

    public static function showPost($slug, $id){
        return Post::find($id)->where(['slug'=>$slug])->with(['comments'=>function($q){
            $q->with('users');
        }])->paginate(1);
    }

    public static function addComments($content, $slug, $user_id)
    {
        $post = Post::where('slug', $slug)->get();
        foreach($post as $p)
        {
            $comments = Comment::create([
            'content'=>$content,
            'id_post'=>$p->id,
            'id_user'=>$user_id,
            ]);
        }
        return $comments;
        // dd($post);
    }

    public function validatePost($request)
    {
        $validator = Validator::make($request, $this->rules);
        if($validator->fails())
        {
            $this->errors = $validator->errors();
            return false;
        }
        return true;
    }

    public function errors()
    {
        return $this->errors;
    }


}
