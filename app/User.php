<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;
use Tymon\JWTAuth\Contracts\JWTSubject;



class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use LaratrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'img', 'nickname' ,'date_of_birth', 'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    protected $table = 'users';

    protected $primaryKey = 'id';

    protected  $rules = [
        'name' => 'required|string|max:255',
        'email' => 'required|email|unique:users',
        'password' => 'required|string|min:3|confirmed',
    ];
    
    public function hasRole($role)
    {
        return $this->role == $role;
    }

    public function comments()
    {   
        return $this->hasMany('App\Comment','user_id','id');
    }

    public function posts()
    {   
        return $this->hasMany('App\Post','user_id','id');
    }

    public function images()
    {
        return $this->hasMany('App\Image', 'user_id', 'id');
    }

    public static function findPostByUser($id)
    {
        return Post::with('users')->where('id', $id)->get();
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
   
}
