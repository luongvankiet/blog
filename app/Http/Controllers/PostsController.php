<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\PostRequest;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Input;
use App\Post;
use App\Category;
use App\User;
use App\Comment;
use Session;
use Auth;
class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $post = Post::where('active',1)->with('users')->paginate(3);
        // $user = User::whereHas('roles')->with('roles')->get();
       

        return view('home',compact('post','users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $post = Post::all();
        return view('posts.create',compact('post'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = Auth::user()->id;
        $post = new Post;
        if(!$post->validatePost($request->all()))
        {
            $errors = $post->errors();
            return redirect()->back()->with('errors',$errors)->withInput();
        }
        
        $post->title = $request->title;
        $post->content = $request->content;
        $post->slug = str_slug($request->title,'-');
        $post->user_id = $user_id;
        $post->category_id = $request->category;
        $post->save();

        Session::flash('success', 'The post was successfully saved.!');

        return redirect()->route('posts.show',['slug'=>$post->slug, 'id'=>$post->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug, $id)
    {
        $post = Post::showPost($slug, $id);
        // dd($post);
        // $cmt = Comment::showCommentOnPost($slug);
        return view('posts.show',compact('post'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
        $post = Post::where('id', $id)->get();
         // dd($post);
        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::find($id);
        $post->title = $request->title;
        $post->content = $request->content;
        $post->slug = str_slug($request->title,'-');
        $post->category_id = $request->category;
        $post->save();
        Session::flash('success', 'The post was successfully saved.!');

        return redirect()->route('posts.show', ['slug'=>$post->slug, 'id'=>$post->id]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->delete();
        Session::flash('success','The post was successfully deleted.!');
        return redirect()->route('posts.index');
    }

    public function showPostByCategory($slug)
    {
        $post = Category::findPostByCategory($slug);
        return view('posts.post-categories',compact('post'));
    }

    public function SortedPost(Request $request, $type)
    {
        if($type == 'users')
        {
            $user_id = $request->user_id;
            $post = Post::where('active',1)->whereHas('users',function($q) use ($user_id){
                $q->where('id', $user_id);
            })->paginate(3);
            $users = User::has('posts')->get();
            return view('home',compact('post','users'));
        }

        $post = Post::where(['active'=> 1,])->with('users')->latest()->paginate(3);
        $users = User::has('posts')->get();
        return view('home',compact('post','users'));
    }

    public function __construct()
    {
         $users = User::has('posts')->get();
        \View::share(['users'=> $users]);
    }
}
