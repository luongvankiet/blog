<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use App\User;

use Illuminate\Support\Facades\Validator;


class LoginController extends Controller
{
    use AuthenticatesUsers;
    
    protected $redirectTo = '/posts';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    
    public function showLoginForm()
    {
        return view('login');
    }

    public function processLogin(Request $request)
    {
        // $user = new User;
        // $validator = Validator::make($request->all(), $user->rules);
        // if($validate ->fails())
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:3'
        ]);
        // {
        //     return redirect()->route('login')->withError('validate');           
        // }
        // else{
            if(Auth::attempt(['email'=>$request->email, 'password'=>$request->password], $request->remember))
            {
                return $this->sendLoginResponse($request);
            }
            return $this->sendFailedLoginResponse($request);;
        // }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }

    
}
