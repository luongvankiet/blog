<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    protected function validateRegister(Request $request)
	{
		$this->validate($request,[
			'name'		=> 'required|string|max:255|',
			'email'		=> 'required|email|unique:users',
			'password'	=> 'required|string|min:3|confirmed'
		]);		
	}

	protected function create(Request $request)
	{
		return User::create([
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'name' => $request->name,
        ]);
	}


	public function showRegisterForm()
	{
		return view('register');
	}

	public function processRegister(Request $request)
	{
		$this->validateRegister($request);
		$user = $this->create($request);
		if(Auth::attempt(['email'=>$request->email, 'password'=>$request->password]))
			return redirect()->route('home');
		return redirect()->route('register')->withInput($request->all());
	}
}
