<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;
use App\User;
use App\Comment;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function home()
    {
        return view('layouts.main');
    }
    public function index()
    {
        $post = Post::where('active', 1)->with('users')->take(3)->get();
        // $user = User::whereHas('roles')->with('roles')->get();
        // dd($cmt);
        return view('home',compact('post'));

    }
    public function view($id)
    {
        $data = User::where('id',$id)->get();
        // $post = User::findPostByUser($id);
        return view('view_member',compact('data'));
        // dd($post);
    }

    
}
