<?php

namespace App\Http\Controllers\ApiControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
use JWTAuthException;

class ApiUsersController extends Controller
{
    	// public function __construct()
     //    {
     //        $this->middleware('auth:api', ['except' => ['login']]);
     //    }

    public function register(Request $request)
    {
    	$validator = Validator::make($request->all(), [
    		'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:3'
        ]);
    	if($validator->fails())
    	{
    		return $validator->errors();
    	}else{
			$user = User::create([
				'name' => $request->get('name'),
				'email' => $request->get('email'),
				'password' => Hash::make($request->get('password'))
	        ]);
	        
	        return response()->json([
	            'status'=> 200,
	            'message'=> 'User created successfully',
	            'data'=>$user
	        ]);
	    }
    }

    public function login(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:3'
        ]);

    	if($validator->fails())
    	{
    		return $validator->errors();
    	}else{
    		$credentials = request(['email', 'password']);
    		try {
		        if (! $token = JWTAuth::attempt($credentials)) {
		            return response()->json(['error' => 'Unauthorized'], 401);
		        }
	    	} catch (JWTAuthException $e) {
	    		return response()->json(['failed_to_create_token'], 500);
        	}
	        return response()->json(compact('token'));
	    }
    }

    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    // protected function respondWithToken($token)
    // {
    //     return response()->json([
    //         'token' => $token,
    //     ]);
    // }

    public function getUserInfo()
    {
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        // the token is valid and we have found the user via the sub claim
        return response()->json(compact('user'));
    }
}
