<?php

use Faker\Generator as Faker;

$factory->define(App\Category::class, function (Faker $faker) {
	$category_name = $faker->jobTitle;
	$slug = str_slug($category_name);
    return [
        //
        'category_name' => $category_name,
        'slug' => $slug,
    ];
});
